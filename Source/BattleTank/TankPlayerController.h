// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BattleTank.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:

	UPROPERTY(EditAnywhere)
	float CrossHairXLocation = 0.5f;

	UPROPERTY(EditAnywhere)
	float CrossHairYLocation = 0.33333f;

	UPROPERTY(EditAnywhere)
	int32 LineTraceRange = 1000000; //default to 10km in cm.

	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	
	ATank * GetControlledTank() const;

	//start tank moving the barrel so that a shot will hit where the crosshair intersects the world.
	void AimTowardCrosshair();

	bool GetSightRayHitLocation(FVector &OUTHitLocation) const;

	bool GetLookVectorHitLocation(FVector &OUTHitLocation) const;
	
};
