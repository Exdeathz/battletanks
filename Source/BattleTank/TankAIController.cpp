// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "Tank.h"
#include "TankAIController.h"




void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	ATank * MyTank = GetControlledTank();

	if (MyTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI Begin Play. Possesing Tank: %s"), *MyTank->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController Begin Play. No Tank Possessed on begin play."));
	}

	ATank * PlayerTank = GetPlayerTank();
	if (PlayerTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI sees player tank: %s"), *PlayerTank->GetName());
	}
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (GetPlayerTank())
	{
		//TODO move toward the player

		//Aim Towards the player
		FVector Hitlocation = GetPlayerTank()->GetActorLocation();
		GetControlledTank()->AimAt(Hitlocation);

		//Fire when ready.
	}
	
}

ATank * ATankAIController::GetControlledTank() const
{
	auto mypawn = GetPawn();
	if (mypawn)
	{
		return Cast<ATank>(mypawn);
	}
	else
	{
		return nullptr;
	}
	
}

ATank * ATankAIController::GetPlayerTank() const
{
	auto playerpawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (playerpawn)
	{
		return Cast<ATank>(playerpawn);
	}
	else
	{
		return nullptr;
	}
	

}