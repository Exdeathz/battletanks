// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "Tank.h"
#include "TankPlayerController.h"



void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	ATank * MyTank = GetControlledTank();

	if (MyTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController Begin Play. Possesing Tank: %s"), *MyTank->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerController Begin Play. No Tank Possessed on begin play."));
	}
}

//Tick
// Called every frame
void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("PlayerController Ticking."));
	AimTowardCrosshair();


}
  
ATank * ATankPlayerController::GetControlledTank() const
{
	auto mypawn = GetPawn();
	if (mypawn)
	{
		return Cast<ATank>(mypawn);
	}
	else
	{
		return nullptr;
	}
}

void ATankPlayerController::AimTowardCrosshair()
{
	if (!GetControlledTank()) { return; }
	FVector HitLocation; //Out parameter.
	if (GetSightRayHitLocation(HitLocation))
	{
		GetControlledTank()->AimAt(HitLocation);
		// Get world location if linetrace through crosshair.
		// If it hits the landscape, then tell the controlled tank to aim guns there.

		//once we hit something, we need to get that point and generate a vector from our turret rotation, and rotate the barrel that way
		// We might also want to figure out what elevation we need on the barrel, and perform that transformation together with the rotation for a smooth transition.
	}

}

bool ATankPlayerController::GetSightRayHitLocation(FVector & OUTHitLocation) const
{

	OUTHitLocation = FVector(0.0f);
	//Get the crosshair position
	int32 viewportsizeX, viewportsizeY;
	GetViewportSize(viewportsizeX, viewportsizeY);

	FVector2D ScreenLocation = FVector2D((viewportsizeX * CrossHairXLocation), (viewportsizeY * CrossHairYLocation));

	FVector CameraLocation; //Have to pass this in, but we will ignore the value.
	FVector WorldDirection;
	if (this->DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraLocation, WorldDirection))
	{
		OUTHitLocation = WorldDirection;
		GetLookVectorHitLocation(OUTHitLocation);
		// line-trace along that direction and see if we hit something.
		
	}

	

	//Return true for a successful aim, return false for an empty aim.
	return true;
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector & OUTHitLocation) const
{
	bool returnvalue;
	FHitResult Hit;
	FVector startlocation = PlayerCameraManager->GetCameraLocation();
	FVector endlocation = startlocation + (OUTHitLocation * LineTraceRange);
	///Setup Query Parameters
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetControlledTank());

	returnvalue = GetWorld()->LineTraceSingleByChannel(Hit, startlocation, endlocation, ECollisionChannel::ECC_Visibility, TraceParameters);
	if (returnvalue)
	{
		OUTHitLocation = Hit.Location;
	}
	else
	{
		OUTHitLocation = FVector(0.0f);
	}
	
	return returnvalue;
}

